/*
** my_ls.c for my_ls in /home/maire_j/rendu/myls-2013-maire_j
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Mon Oct 28 11:54:07 2013 Maire Jonathan
** Last update Mon Oct 28 15:20:45 2013 Maire Jonathan
*/

#include "myls.h"
#include "libmy/my.h"
#include <dirent.h>
#include <stdlib.h>

int	sort_stuff(char **files)
{
  int	i;
  int	j;

  i = 0;
  j = 0;
  i = j + i;
  return (i);
}


char	**ls_no_arg()
{
  DIR   *dirp;
  char	**files;
  int	i;

  i = 0;
  files = malloc(my_put_nbr(count_dir(".")) + 1);
  dirp = opendir(".");
  if(dirp == 0 || files == 0)
    {
      return (1);
    }
  while((pdir = readdir(dirp)))
    {
      if (pdir->d_type == 4)
	{
	  my_putstr("\033[1;34m");
	  files[i] = my_strdup(pdir->d_name);
	  my_putstr(files[i]);
          my_putstr("\033[m");
	}
      else
	{
	  files[i] = my_strdup(pdir->d_name);
	  my_putstr(files[i]);
	}
      i = i + 1;
      my_putstr(" ");
    }
  files[i] = '\0';
  closedir(dirp);
  return (files);
}

int	count_dir(char *dir)
{
  DIR	*dirp;
  int	res;

  res = 0;
  dirp = opendir(dir);
  if (dirp == 0)
    {
      return (-1);
    }
  while ((pdir = readdir(dirp)))
    {
      res = res + 1;
    }
  closedir(dirp);
  return (res);
}

int	main(int ac, char **av)
{
  ls_no_arg();
  my_putchar('\n');
  return (0);
}
