/*
** get_count.c in /home/maire_j/rendu/myls-2013-maire_j
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Mon Oct 28 10:58:51 2013 Maire Jonathan
** Last update Mon Oct 28 12:11:10 2013 Maire Jonathan
*/

#include <stdlib.h>
#include "libmy/my.h"

int	count_files(int ac, char **av)
{
  int	i;
  int	res;

  i = 1;
  res = 0;
  while (i < ac)
    {
      if (av[i][0] != '-')
        {
          res = res + 1;
        }
      i = i + 1;
    }
  return (res);
}

char	**get_files_name(int ac, char **av)
{
  int	i;
  int	res;
  char	**result;

  i = 1;
  res = 0;
  result = malloc(count_files(ac, av) * sizeof(char) + 1);
  while (i < ac)
    {
      if (av[i][0] != '-' && result != 0)
	{
	  result[res] = my_strdup(av[i]);
	  res = res + 1;
	}
      i = i + 1;
    }
  result[res] = '\0';
  return (result);
}

int	count_command(int ac, char **av)
{
  int	i;
  int	res;

  i = 1;
  res = 0;
  while (i < ac)
    {
      if (av[i][0] == '-')
	{
	  res = res + 1;
	}
      i = i + 1;
    }
  return (res);
}

char	**get_commands(int ac, char **av)
{
  int	i;
  int	res;
  char	**commands;

  i = 1;
  res = 0;
  commands = malloc(count_command(ac, av) + 1);
  while (i < ac)
    {
      if (av[i][0] == '-' && commands != 0)
	{
	  commands[res] = my_strdup(av[i]);
	  res = res + 1;
	}
      i = i + 1;
    }
  commands[res] = '\0';
  return (commands);
}

void	process_args(int ac, char **av)
{
  char **commands;
  char **files;
  int	i;

  files = get_files_name(ac, av);
  commands = get_commands(ac, av);
  i = 0;
  while (files[i])
    {
      my_putstr(files[i]);
      i = i + 1;
    }
  i = 0;
  while (commands[i])
    {
      my_putstr(commands[i]);
      i = i + 1;
    }
}
