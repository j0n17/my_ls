/*
** my_strlen.c for my_strlen in /home/maire_j/rendu/Piscine-C-lib/my
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Tue Oct  8 07:06:39 2013 Maire Jonathan
** Last update Tue Oct  8 07:07:25 2013 Maire Jonathan
*/

int	my_strlen(char *str)
{
  int	i;
  
  i = 0;
  while (str[i] != '\0')
    {
      i = i + 1;
    }
  return (i);
}
