/*
** my_strupcase.c for lib in /home/maire_j/rendu/Piscine-C-lib
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Fri Oct 11 14:51:43 2013 Maire Jonathan
** Last update Fri Oct 11 14:59:46 2013 Maire Jonathan
*/

char	*my_strupcase(char *str)
{
  int	i;
  
  i = 0;
  while (str[i])
    {
      if (str[i] >= 'a' && str[i] <= 'z')
	{
	  str[i] = str[i] - 32;
	}
      i = i + 1;
    }
  return (str);
}
